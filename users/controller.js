const UserModel = require("./model");

// Untuk mendapatkan semua user yang tersimpan
class userController {
  getAllUser = async (req, res) => {
    const allUsers = await UserModel.getAllUser();
    return res.json(allUsers);
  };

  // Untuk menampilkan data
  getSingleUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const singleUser = await UserModel.getSingleUser(idUser);
      if (singleUser) {
        console.log(singleUser);
        return res.json(singleUser);
      } else {
        res.statusCode = 400;
        return res.json({ message: "User ID " + idUser + " tidak ditemukan." });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "User ID tidak ditemukan: " + idUser });
    }
  };

  // // Untuk update User
  // updateSingle = async (req, res) => {
  //   const { idUser } = req.params;
  //   const { username } = req.body;
  //   const { email } = req.body;
  //   const userUpdate = await UserModel.updateSingleBio(idUser);
  //   console.log(userUpdate);
  //   return res.json(userUpdate);
  // };

  // Untuk menampilkan data bio
  getSingleUserBio = async (req, res) => {
    const { idUser } = req.params;
    try {
      const singleUserBio = await UserModel.getSingleUserBio(idUser);
      if (singleUserBio) {
        return res.json(singleUserBio);
      } else {
        res.statusCode = 400;
        return res.json({
          message: "User ID " + idUser + " tidak ditemukan.",
        });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "User ID tidak ditemukan: " + idUser });
    }
  };

  // // Untuk update User Bio
  // updateSingleBio = async (req, res) => {
  //   const { user_ID } = req.params;
  //   const { namaLengkap } = req.body;
  //   const { alamat } = req.body;
  //   const { noTel } = req.body;
  //   const { tanggalLahir } = req.body;
  //   try {
  //     const userBioUpdate = await UserModel.updateSingleBio(user_ID);
  //     if (userBioUpdate) {
  //       return res.json(userBioUpdate);
  //     } else {
  //       res.statusCode = 400;
  //       return res.json({
  //         message: "User ID " + user_ID + " tidak ditemukan.",
  //       });
  //     }
  //   } catch (error) {
  //     res.statusCode = 400;
  //     return res.json({ message: "User ID tidak ditemukan: " + user_ID });
  //   }
  // };

  // Untuk update User Bio
  updateSingleBio = async (req, res) => {
    const { idUser } = req.params;
    const dataUpdateBio = req.body;
    // const { namaLengkap } = req.body;
    // const { alamat } = req.body;
    // const { noTel } = req.body;
    // const { tanggalLahir } = req.body;
    const userBioUpdate = await UserModel.updateSingleBio(
      idUser,
      dataUpdateBio
    );
    return res.json({ message: "Oke deh, update sudah tersimpan ^_^" });
  };

  // Untuk pengisian data-data User
  registeredUser = async (req, res) => {
    const dataRequest = req.body;

    if (dataRequest.username === undefined || dataRequest.username === "") {
      res.statusCode = 400;
      return res.json({ message: "Username jangan lupa diisi donk :(" });
    }

    if (dataRequest.email === undefined || dataRequest.email === "") {
      res.statusCode = 400;
      return res.json({ message: "Email jangan lupa diisi donk :(" });
    }

    if (dataRequest.password === undefined || dataRequest.password === "") {
      res.statusCode = 400;
      return res.json({ message: "Password jangan lupa diisi donk :(" });
    }

    const existData = await UserModel.isUserRegistered(dataRequest);

    if (existData) {
      res.statusCode = 400;
      return res.json({
        message:
          "Username atau email sudah pernah terdaftar. Ganti yang lain ya :)",
      });
    }

    UserModel.recordNewData(dataRequest);
    return res.json({ message: "Oke deh, sudah tersimpan ^_^" });
  };

  // Untuk pengisian data-data User bio
  registeredUserBio = async (req, res) => {
    const dataBioRequest = req.body;

    UserModel.recordNewDataBio(dataBioRequest);
    return res.json({ message: "Oke deh, sudah tersimpan ^_^" });
  };

  // Untuk login
  userLogin = async (req, res) => {
    const { username, password } = req.body;
    const dataLogin = await UserModel.verifyLogin(username, password);
    if (dataLogin) {
      return res.json(dataLogin);
    } else {
      return res.json({ message: "Uppsss salah >_<" });
    }
  };

  // Untuk pengisian data game
  gameHistory = async (req, res) => {
    const dataGameRequest = req.body;

    UserModel.recordNewDataGame(dataGameRequest);
    return res.json({ message: "Oke deh, sudah tersimpan ^_^" });
  };
}

module.exports = new userController();
