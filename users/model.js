const userList = [];
const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

class UserModel {
  getAllUser = async () => {
    return await db.pengguna.findAll({ include: [db.penggunaBio] });
  };

  getSingleUser = async (idUser) => {
    return await db.pengguna.findOne({ where: { id: idUser } });
  };

  // updateSingle = async (idUser) => {
  //   return await db.pengguna.findOne({ where: { id: idUser } });
  // };

  getSingleUserBio = async (idUser) => {
    return await db.penggunaBio.findOne({ where: { id: idUser } });
  };

  updateSingleBio = async (idUser, dataUpdateBio) => {
    return await db.penggunaBio.update(
      {
        namaLengkap: dataUpdateBio.namaLengkap,
        alamat: dataUpdateBio.alamat,
        noTel: dataUpdateBio.noTel,
        tanggalLahir: dataUpdateBio.tanggalLahir,
      },
      { where: { id: idUser } }
    );
  };

  isUserRegistered = async (dataRequest) => {
    const existData = await db.pengguna.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  recordNewData = (dataRequest) => {
    db.pengguna.create({
      username: dataRequest.username,
      email: dataRequest.email,
      password: md5(dataRequest.password),
    });
  };

  recordNewDataBio = (dataBioRequest) => {
    db.penggunaBio.create({
      namaLengkap: dataBioRequest.namaLengkap,
      alamat: dataBioRequest.alamat,
      noTel: dataBioRequest.noTel,
      tanggalLahir: dataBioRequest.tanggalLahir,
    });
  };

  recordNewDataGame = (dataGameRequest) => {
    db.game.create({
      status: dataGameRequest.status,
      waktu: dataGameRequest.waktu,
    });
  };

  verifyLogin = async (username, password) => {
    const dataUser = await db.pengguna.findOne({
      where: {
        username: username,
        password: md5(password),
      },
    });

    return dataUser;
  };
}

module.exports = new UserModel();
